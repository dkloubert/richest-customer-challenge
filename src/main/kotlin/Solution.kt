fun maximumWealth(accounts: Array<IntArray>): Int =
    accounts.maxOfOrNull { it.sum() } ?: 0

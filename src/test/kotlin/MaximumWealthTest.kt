import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test
import org.junit.jupiter.params.ParameterizedTest
import org.junit.jupiter.params.provider.*
import java.util.stream.Stream

class MaximumWealthTest {

    @Test
    fun `it returns 0 when no accounts are there`() {
        val result = maximumWealth(emptyArray())

        assertThat(result).isEqualTo(0)
    }

    @Test
    fun `it returns 0 when the accounts are empty`() {
        val result = maximumWealth(arrayOf(intArrayOf()))
        assertThat(result).isEqualTo(0)
    }

    @ParameterizedTest(name = "it returns the highest wealth for {arguments}")
    @MethodSource("testData")
    fun `it returns the highest wealth`(accounts: Array<IntArray>, expectedResult: Int) {
        val result = maximumWealth(accounts)
        assertThat(result).isEqualTo(expectedResult)
    }

    companion object {
        @JvmStatic
        fun testData(): Stream<Arguments> {
            return Stream.of(
                Arguments.of(arrayOf(intArrayOf(2, 8, 7), intArrayOf(7, 1, 3), intArrayOf(1, 9, 5)), 17),
                Arguments.of(arrayOf(intArrayOf(1, 2, 3), intArrayOf(3, 2, 1)), 6),
                Arguments.of(arrayOf(intArrayOf(1, 5), intArrayOf(7, 3), intArrayOf(3, 5)), 10),
                Arguments.of(arrayOf(intArrayOf(1, 5), intArrayOf(7, 3), intArrayOf(3, 5), intArrayOf(1, 2, 3)), 10)
            )
        }
    }
}
